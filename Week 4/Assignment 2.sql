--Create database Product;

Use Product;
Select * from products;
--Create table products(
--    productId INT primary key,
--    productName varchar(50),
--    productBrand varchar(50),
--    PRoductPrice INT
--);

--Insert into products values
--(1,'coalgate','cibaca',50),
--(2,'Brush','coalgate',60),
--(3,'Soap','Numberone',20),
--(4,'abc','cbc',70),
--(5,'net','jindal',50),
--(6,'wire','pvc',50),
--(7,'chain','Steels',50);

Select * From
products

Order By productName

OFFSET 2 ROWS
FETCH NEXT 2 ROWS ONLY;


--Output

productId	productName	productBrand	PRoductPrice
1	coalgate	cibaca	50
2	Brush	coalgate	60
3	Soap	Numberone	20
4	abc	cbc	70
5	net	jindal	50
6	wire	pvc	50
7	chain	Steels	50


productId	productName	productBrand	PRoductPrice
7	chain	Steels	50
1	coalgate	cibaca	50


--Lead Function

Select productName,PRoductPRice,
Lead (PRoductPrice)  OVER (ORDER BY PRoductPrice ) as nextProductPrice
From products;

--Output
productName	PRoductPRice	nextProductPrice
Soap	20	50
coalgate	50	50
net	50	50
wire	50	50
chain	50	60
Brush	60	70
abc	70	NULL




--Lag Function

Select productName,PRoductPRice,
Lag (PRoductPrice)  OVER (ORDER BY PRoductPrice ) as previousProductPrice
From products;


--Output
productName	PRoductPRice	previousProductPrice
Soap	20	NULL
coalgate	50	20
net	50	50
wire	50	50
chain	50	50
Brush	60	50
abc	70	60