Use Employee;

----CREATE TABLE SALES
----(
----       SALE_ID        INTEGER,
----       PRODUCT_ID     INTEGER,
----       YEAR           INTEGER,
----       Quantity       INTEGER,
----       PRICE          INTEGER
----);


--INSERT INTO SALES VALUES ( 1, 100, 2008, 10, 5000);
--INSERT INTO SALES VALUES ( 2, 100, 2009, 12, 5000);
--INSERT INTO SALES VALUES ( 3, 100, 2010, 25, 5000);
--INSERT INTO SALES VALUES ( 4, 100, 2011, 16, 5000);
--INSERT INTO SALES VALUES ( 5, 100, 2012, 8,  5000);

--INSERT INTO SALES VALUES ( 6, 200, 2010, 10, 9000);
--INSERT INTO SALES VALUES ( 7, 200, 2011, 15, 9000);
--INSERT INTO SALES VALUES ( 8, 200, 2012, 20, 9000);
--INSERT INTO SALES VALUES ( 9, 200, 2008, 13, 9000);
--INSERT INTO SALES VALUES ( 10,200, 2009, 14, 9000);

--INSERT INTO SALES VALUES ( 11, 300, 2010, 20, 7000);
--INSERT INTO SALES VALUES ( 12, 300, 2011, 18, 7000);
--INSERT INTO SALES VALUES ( 13, 300, 2012, 20, 7000);
--INSERT INTO SALES VALUES ( 14, 300, 2008, 17, 7000);
--INSERT INTO SALES VALUES ( 15, 300, 2009, 19, 7000);
--COMMIT;

SELECT * FROM SALES;

--1. Write a query to find the number of products sold in each year?

Select YEAR, count(PRODUCT_ID) as CNT
FROM SALES
GROUP BY YEAR;
GO


Select SALE_ID, PRODUCT_ID, YEAR, Quantity, PRICE, 
count(PRODUCT_ID) over (partition by YEAR) as CNT
From SALES;
Go

--2. Write a SQL query using the analytic function to find the total sales(QUANTITY) of each product?

Select PRODUCT_ID, Quantity, 
SUM(Quantity) over (partition by PRODUCT_ID) as TOT_SALES
From SALES;
Go



----3. Write a SQL query to find the cumulative sum of sales(QUANTITY) of each product? Here first sort the QUANTITY in ascendaing order for each product and then accumulate the QUANTITY.
--Cumulative sum of QUANTITY for a product = QUANTITY of current row + sum of QUANTITIES all previous rows in that product.


Select PRODUCT_ID, Quantity, Sum(Quantity) 
             Over ( Partition By PRODUCT_ID  Order By Quantity ) As CUM_SALES
From SALES;
Go

----4
--4. Write a SQL query to find the sum of sales of current row and previous 2 rows in a product group? Sort the data on sales and then find the sum.


SELECT PRODUCT_ID, Quantity,
LAG(Quantity,0,0) over (PARTITION by PRODUCT_ID order by Quantity desc) +
LAG(Quantity,1,0) over (PARTITION by PRODUCT_ID order by Quantity desc) +
LAG(Quantity,2,0) over (PARTITION by PRODUCT_ID order by Quantity desc)
as CAL_SALES
FROM SALES;
GO


--5 
--5. Write a SQL query to find the Median of sales of a product?


SELECT PRODUCT_ID, Quantity,
MEDIAN = PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY Quantity) OVER (PARTITION BY PRODUCT_ID)
FROM SALES;
GO


--6 Write a SQL query to find the minimum sales of a product without using the group by clause.


SELECT PRODUCT_ID, YEAR, QUANTITY from
	(SELECT PRODUCT_ID, YEAR,
	 MIN(Quantity) OVER (PARTiTION BY PRODUCT_ID) as QUANTITY,
	 ROW_NUMBER() over (PARTiTION BY PRODUCT_ID order by Quantity ) as RNO
	 FROM SALES) as t
WHERE RNO = 1;
Go 


