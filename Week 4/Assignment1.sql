Use Employee;

create table EMP (id int primary key identity(1,1), Fname varchar(20), Lname varchar(20), Location varchar(20));
create table EMPDeatails (EmpId Nvarchar(100) , Name varchar(100), DOJ date, Location varchar(20), id int foreign key references EMP(id) )
Go

create trigger InsertEMPDetails on EMP
After insert AS
Begin
	declare @fname varchar(20), @lname varchar(20), @location varchar(20), @id int;
	SELECT @fname = Fname, @lname = Lname, @location = Location, @id = id from INSERTED;

	Insert into EMPDeatails values 
		(concat(@fname,@id),concat(@fname,' ',@lname), GETDATE(), @location, @id);

	print 'A new employee is added!'
End
GO


insert into EMP values ('Himanshu', 'Dadhich', 'Jaipur');
GO
insert into EMP values ('Arun', 'Mehta', 'Delhi');
Go
Select * from EMP;
GO
Select * from EMPDeatails;
GO

DRop table EMPDeatails;
Drop table EMP;

--Output
(1 row affected)
A new employee is added!

(1 row affected)

(1 row affected)
A new employee is added!

(1 row affected)
id          Fname                Lname                Location
----------- -------------------- -------------------- --------------------
1           Himanshu             Dadhich              Jaipur
2           Arun                 Mehta                Delhi

(2 rows affected)

EmpId                                                                                                Name                                                                                                 DOJ        Location             id
---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ---------- -------------------- -----------
Himanshu1                                                                                            Himanshu Dadhich                                                                                     2021-07-19 Jaipur               1
Arun2