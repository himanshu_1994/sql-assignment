--Use Employee;

--Create Table Transactions(
--ID INT ,
--Value INT,


--);
Create Procedure NewTransactions

@pID INT,
   @pNewValue INT 
AS
BEGIN
 
   BEGIN TRY
 
      BEGIN TRANSACTION
 
         UPDATE Transactions
         SET Value=@pNewValue
         WHERE ID=@pID AND Value < 100
 
      IF @@TRANCOUNT > 0
         COMMIT
 
   END TRY

   BEGIN CATCH 

      IF @@TRANCOUNT > 0
         ROLLBACK
 
      SELECT ERROR_NUMBER() AS ErrorNumber
      SELECT ERROR_MESSAGE() AS ErrorMessage
 
   END CATCH
END
